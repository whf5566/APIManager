# APIManager
APIManager是一个简单的，基于AFNetworking（https://github.com/AFNetworking/AFNetworking）的网络请求管理库。网络请求完成后，可以block形式回调，也可以delegate形式回调

## Installation
代码依赖于AFNetworking，首先需要在工程中加入AFNetworking,然后将文件夹加入到工程中即可。


## License
All code is licensed under the MIT license. See the LICENSE file for more details.