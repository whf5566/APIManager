//
//  APIManager.m
//
//  Created by whf5566@gmail.com on 15/8/17.
//  Copyright (c) 2015. https://github.com/whf5566
//

#import "APIManager.h"
@interface APIManager ()
@property (strong, nonatomic) AFURLSessionManager                                   *sessionManager;
@property (nonatomic, strong) AFHTTPRequestSerializer <AFURLRequestSerialization>   *requestSerializer; // 不能为空
@end

@implementation APIManager
- (id)init
{
    self = [super init];

    if (self) {
        _sessionManager = [[AFURLSessionManager alloc] init];
        _requestSerializer = [AFHTTPRequestSerializer serializer];
        _sessionManager.responseSerializer = [AFJSONResponseSerializer serializer];
        _domain = URL_SERVER;
    }

    return self;
}

#pragma mark -
+ (APIManager *)manager
{
    static dispatch_once_t  once;
    static APIManager       *__manager;

    dispatch_once(&once, ^() {
        __manager = [[APIManager alloc] init];
    });
    return __manager;
}

#pragma mark - Public
- (APITask *)callApiID:(NSInteger)apiID param:(id)param delegate:(id <APITaskDelegate>)delegate
{
    APIInfo *apiInfo = [self apiWithIndetify:apiID];

    if (apiInfo) {
        [apiInfo addParam:param];
    }

    return [self callApi:apiInfo delegate:delegate];
}

- (APITask *)callApi:(APIInfo *)api delegate:(id <APITaskDelegate>)delegate
{
    if (api == nil) {
        if ([delegate respondsToSelector:@selector(APITaskDidFailed:error:)]) {
            dispatch_async(self.sessionManager.completionQueue ? : dispatch_get_main_queue(), ^{
                [delegate APITaskDidFailed:nil error:[NSError APIError:APIErrorWrongParam userInfo:nil]];
            });
        }

        return nil;
    }

    if (api.requestSerializer == nil) {
        // 没有自定义的requestSerializer，使用默认的
        api.requestSerializer = self.requestSerializer;
    }

    NSError         *serializationError = nil;
    NSURLRequest    *request = [api requestWithError:&serializationError];

    if (serializationError) {
        if ([delegate respondsToSelector:@selector(APITaskDidFailed:error:)]) {
            dispatch_async(self.sessionManager.completionQueue ? : dispatch_get_main_queue(), ^{
                [delegate APITaskDidFailed:nil error:serializationError];
            });
        }

        return nil;
    }

    __block APITask         *apiTask = nil;
    NSURLSessionDataTask    *dataTask = [self.sessionManager dataTaskWithRequest:request completionHandler:^(NSURLResponse *__unused response, id responseObject, NSError *error) {
            if (error) {
                if ([apiTask.delegate respondsToSelector:@selector(APITaskDidFailed:error:)]) {
                    [apiTask.delegate APITaskDidFailed:apiTask error:error];
                }
            } else {
                if ([apiTask.delegate respondsToSelector:@selector(APITaskDidSuccess:response:)]) {
                    [apiTask.delegate APITaskDidSuccess:apiTask response:responseObject];
                }
            }
        }];
    apiTask = [[APITask alloc] initWithApi:api sesstionTask:dataTask delegate:delegate];
    [apiTask resume];
    return apiTask;
}

- (APITask *)callApiID:(NSInteger)apiID param:(id)param sucess:(APITaskDidSucess)success fail:(APITaskDidFailed)fail
{
    APIInfo *apiInfo = [self apiWithIndetify:apiID];

    if (apiInfo) {
        [apiInfo addParam:param];
    }

    return [self callApi:apiInfo sucess:success fail:fail];
}

- (APITask *)callApi:(APIInfo *)api sucess:(APITaskDidSucess)success fail:(APITaskDidFailed)fail
{
    if (api == nil) {
        if (fail) {
            dispatch_async(self.sessionManager.completionQueue ? : dispatch_get_main_queue(), ^{
                fail(nil, [NSError APIError:APIErrorWrongParam userInfo:nil]);
            });
        }

        return nil;
    }

    if (api.requestSerializer == nil) {
        // 没有自定义的requestSerializer，使用默认的
        api.requestSerializer = self.requestSerializer;
    }

    NSError         *serializationError = nil;
    NSURLRequest    *request = [api requestWithError:&serializationError];

    if (serializationError) {
        if (fail) {
            dispatch_async(self.sessionManager.completionQueue ? : dispatch_get_main_queue(), ^{
                fail(nil, serializationError);
            });
        }

        return nil;
    }

    __block APITask         *apiTask = nil;
    NSURLSessionDataTask    *dataTask = [self.sessionManager dataTaskWithRequest:request completionHandler:^(NSURLResponse *__unused response, id responseObject, NSError *error) {
            if (error) {
                if (fail) {
                    fail(apiTask, error);
                }
            } else {
                if (success) {
                    success(apiTask, responseObject);
                }
            }
        }];
    apiTask = [[APITask alloc] initWithApi:api sesstionTask:dataTask delegate:nil];
    [apiTask resume];
    return apiTask;
}

#pragma mark Private

- (APIInfo *)apiWithIndetify:(NSInteger)appID
{
    if (appID < 0) {
        return nil;
    }

    NSString *url = [self urlWithAPIIndetify:appID];

    APIInfo *apiInfo = [[APIInfo alloc] initWithID:appID method:HTTPGET url:url];
    [apiInfo addHeader:self.commonHeader];
    [apiInfo addParam:self.commonParam];
    return apiInfo;
}

- (NSString *)urlWithAPIIndetify:(NSInteger)appID
{
    NSString *url = API_URL()[@(appID)];

    if ([url hasPrefix:@"http"]) {
        return url;
    }

    NSString *requestUrl = [NSString stringWithFormat:@"http://%@%@", self.domain, url ? url : @""];

    return requestUrl;
}

@end