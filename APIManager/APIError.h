//
//  APIError.h
//
//  Created by whf5566@gmail.com on 15/8/17.
//  Copyright (c) 2015. https://github.com/whf5566
//

#import <Foundation/Foundation.h>

extern NSString *const APIErrorDomain;

typedef NS_ENUM (NSInteger, APIErrorCode) {
    APIErrorUnknown = 1000000L,
    APIErrorWrongParam, /*参数错误*/

    APIInfoError = 2000000L,
    APIInfoErrorNORequestSerializer, /*没有序列器*/
};

@interface NSError (APIError)
+ (NSError *)APIError:(APIErrorCode)code userInfo:(NSDictionary *)dict;
@end