//
//  APIInfo.h
//
//  Created by whf5566@gmail.com on 15/8/17.
//  Copyright (c) 2015. https://github.com/whf5566
//

#import <Foundation/Foundation.h>
#import "APIIndetify.h"
@protocol AFURLRequestSerialization;



@interface APIInfo : NSObject
@property (assign, nonatomic) APIIdentify       apiID;
@property (copy, nonatomic) NSString            *URLString;
@property (copy, nonatomic) NSString            *HTTPMethod; // 默认为GET
@property (assign, nonatomic) NSTimeInterval    timeoutInterval;

@property (strong, nonatomic) id <AFURLRequestSerialization> requestSerializer;

- (instancetype)initWithID:(APIIdentify)apiID method:(NSString *)method url:(NSString *)url;

- (NSURLRequest *)requestWithError:(NSError *__autoreleasing *)error;
- (void)addHeader:(NSDictionary *)header;
- (void)addParam:(NSDictionary *)param;
@end

extern NSString *const  HTTPGET;
extern NSString *const  HTTPPOST;
extern NSString *const  HTTPHEAD;
extern NSString *const  HTTPPUT;
extern NSString *const  HTTPPATCH;
extern NSString *const  HTTPDELETE;