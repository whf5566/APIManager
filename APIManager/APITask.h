//
//  APITask.h
//
//  Created by whf5566@gmail.com on 15/8/17.
//  Copyright (c) 2015. https://github.com/whf5566
//

#import <Foundation/Foundation.h>

typedef NS_ENUM (NSInteger, APITaskState) {
    APITaskStateRunning = 0,
    APITaskStateSuspended = 1,
    APITaskStateCanceling = 2,
    APITaskStateCompleted = 3,
};

@class APIInfo;
@protocol APITaskDelegate;

/**
 *  发起API请求后，生成的API请求任务类;可以暂停、取消、开始这个任务
 */
@interface APITask : NSObject
@property (readonly, assign, nonatomic) NSInteger       apiID;
@property (readonly, strong, nonatomic) APIInfo         *apiInfo;
@property (readonly, copy, nonatomic) NSString          *taskDescription;
@property (readonly, assign, nonatomic) APITaskState    state;

@property (weak, nonatomic) id <APITaskDelegate> delegate;

- (instancetype)initWithApi:(APIInfo *)api sesstionTask:(NSURLSessionTask *)task delegate:(id <APITaskDelegate>)delegate;

- (void)cancel;
- (void)suspend;
- (void)resume;
@end

/**
 *  任务成功后的回调
 */
@protocol APITaskDelegate <NSObject>

- (void)APITaskDidSuccess:(APITask *)task response:(id)responseObject;
- (void)APITaskDidFailed:(APITask *)task error:(NSError *)error;

@end