//
//  APIIndetify.m
//
//  Created by whf5566@gmail.com on 15/8/17.
//  Copyright (c) 2015. https://github.com/whf5566
//

#import "APIIndetify.h"

NSDictionary *API_URL()
{
    static dispatch_once_t  once;
    static NSDictionary     *_url;
    
    dispatch_once(&once, ^() {
        _url = @{
                 @(API_Login):@"/Public/login",
                 };
    });
    return _url;
}