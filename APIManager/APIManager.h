//
//  APIManager.h
//
//  Created by whf5566@gmail.com on 15/8/17.
//  Copyright (c) 2015. https://github.com/whf5566
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>
#import "APITask.h"
#import "APIInfo.h"
#import "APIIndetify.h"
#import "APIError.h"
#import "AppManager.h"
typedef void (^ APITaskDidSucess)(APITask *task,id responseObject);
typedef void (^ APITaskDidFailed)(APITask *task,NSError *error);


@interface APIManager : NSObject
@property (copy,nonatomic) NSString *domain;
@property (copy,nonatomic) NSDictionary *commonHeader;
@property (copy,nonatomic) NSDictionary *commonParam;

+ (APIManager *)manager;

- (APITask*)callApiID:(NSInteger)apiID param:(id)param delegate:(id<APITaskDelegate>)delegate;
- (APITask*)callApi:(APIInfo*)api delegate:(id<APITaskDelegate>)delegate;

- (APITask*)callApiID:(NSInteger)apiID param:(id)param sucess:(APITaskDidSucess)success fail:(APITaskDidFailed)fail;
- (APITask*)callApi:(APIInfo*)api sucess:(APITaskDidSucess)success fail:(APITaskDidFailed)fail;

@end
