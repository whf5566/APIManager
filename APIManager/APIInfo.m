//
//  APIInfo.m
//
//  Created by whf5566@gmail.com on 15/8/17.
//  Copyright (c) 2015. https://github.com/whf5566
//

#import "APIInfo.h"
#import "APIError.h"

#import <AFURLRequestSerialization.h>

@interface APIInfo ()
@property (copy, nonatomic) NSDictionary    *header;
@property (copy, nonatomic) NSDictionary    *param;
@end

@implementation APIInfo

#pragma mark -
- (instancetype)init
{
    self = [super init];

    if (self) {
        _apiID = API_Unknown;
        _URLString = @"";
        _HTTPMethod = HTTPGET;
        _timeoutInterval = 60.0;
        _requestSerializer = nil;
        _header = nil;
        _param = nil;
    }

    return self;
}

- (instancetype)initWithID:(APIIdentify)apiID method:(NSString *)method url:(NSString *)url
{
    self = [self init];

    if (self) {
        _apiID = apiID;
        _URLString = url;
        _HTTPMethod = method;
    }

    return self;
}

#pragma mark - Public

- (NSURLRequest *)requestWithError:(NSError *__autoreleasing *)error
{
    NSParameterAssert(_HTTPMethod);
    NSParameterAssert(_URLString);

    NSURL *url = [NSURL URLWithString:_URLString];

    NSParameterAssert(url);

    NSMutableURLRequest *mutableRequest = [[NSMutableURLRequest alloc] initWithURL:url];
    mutableRequest.HTTPMethod = _HTTPMethod;

    [self.header enumerateKeysAndObjectsUsingBlock:^(id field, id value, BOOL *__unused stop) {
        if (![mutableRequest valueForHTTPHeaderField:field]) {
            [mutableRequest setValue:value forHTTPHeaderField:field];
        }
    }];

    if ([self.requestSerializer respondsToSelector:@selector(requestBySerializingRequest:withParameters:error:)]) {
        mutableRequest = [[self.requestSerializer requestBySerializingRequest:mutableRequest withParameters:self.param error:error] mutableCopy];
        mutableRequest.timeoutInterval = _timeoutInterval;
    } else {
        mutableRequest = nil;
        *error = [NSError APIError:APIInfoErrorNORequestSerializer userInfo:nil];
    }

    return mutableRequest;
}

- (void)addHeader:(NSDictionary *)header
{
    if ((header == nil) || (header.count == 0)) {
        return;
    }

    if (self.header == nil) {
        self.header = header;
    } else {
        NSMutableDictionary *dic = [self.header mutableCopy];
        [dic addEntriesFromDictionary:header];
        self.header = dic;
    }
}

- (void)addParam:(NSDictionary *)param
{
    if ((param == nil) || (param.count == 0)) {
        return;
    }

    if (self.param == nil) {
        self.param = param;
    } else {
        NSMutableDictionary *dic = [self.param mutableCopy];
        [dic addEntriesFromDictionary:param];
        self.param = dic;
    }
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"<%@: %p, APIID: %d, URL: %@, HTTPMethod: %@, timeoutInterval:%lf,HEADER:%@ ,PARAM:%@ >", NSStringFromClass([self class]), self, (int)self.apiID, self.URLString, self.HTTPMethod, self.timeoutInterval, self.header, self.param];
}

@end

NSString *const HTTPGET = @"GET";
NSString *const HTTPPOST = @"POST";
NSString *const HTTPHEAD = @"HEAD";
NSString *const HTTPPUT = @"PUT";
NSString *const HTTPPATCH = @"PATCH";
NSString *const HTTPDELETE = @"DELETE";