//
//  APIError.m
//
//  Created by whf5566@gmail.com on 15/8/17.
//  Copyright (c) 2015. https://github.com/whf5566
//

#import "APIError.h"

NSString *const  APIErrorDomain = @"APIErrorDomain";


@implementation NSError (APIError)
+ (NSError*)APIError:(APIErrorCode)code userInfo:(NSDictionary *)dict
{
    return [NSError errorWithDomain:APIErrorDomain code:code userInfo:dict];
}

@end
