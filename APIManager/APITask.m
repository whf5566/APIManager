//
//  APITask.m
//
//  Created by whf5566@gmail.com on 15/8/17.
//  Copyright (c) 2015. https://github.com/whf5566
//

#import "APITask.h"
#import "APIInfo.h"

@interface APITask ()
@property (readwrite,strong,nonatomic) APIInfo  *apiInfo;
@property (strong, nonatomic) NSURLSessionTask  *sessionTask;
@end

@implementation APITask
#pragma mark - 
- (instancetype)initWithApi:(APIInfo *)api sesstionTask:(NSURLSessionTask *)task delegate:(id <APITaskDelegate>)delegate
{
    self = [super init];

    if (self) {
        self.apiInfo = api;
        self.sessionTask = task;
        self.delegate = delegate;
    }

    return self;
}

#pragma mark - Public
- (NSInteger)apiID
{
    return self.apiInfo.apiID;
}

- (void)cancel
{
    [self.sessionTask cancel];
}

- (void)suspend
{
    [self.sessionTask suspend];
}

- (void)resume
{
    [self.sessionTask resume];
}

- (APITaskState)state
{
    APITaskState state = APITaskStateRunning;

    switch (self.sessionTask.state) {
        case NSURLSessionTaskStateRunning:
            state = APITaskStateRunning;
            break;

        case NSURLSessionTaskStateCanceling:
            state = APITaskStateCanceling;
            break;

        case NSURLSessionTaskStateCompleted:
            state = APITaskStateCompleted;
            break;

        case NSURLSessionTaskStateSuspended:
            state = APITaskStateSuspended;
            break;

        default:
            break;
    }
    return state;
}

- (NSString *)taskDescription
{
    return self.sessionTask.taskDescription;
}

@end